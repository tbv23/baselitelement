import { LitElement, html } from "lit-element";
import '../persona-ficha-listado/persona-ficha-listado.js';

class PersonaMain extends LitElement {

    static get properties() {
      return {
        people: {type: Array}
      };
    }

    constructor() {
      super();

      this.people = [
        {
          name: "Ellen Ripley",
          yearsInCompany: 10,
          photo: {
            src: "./img/persona.jpg",
            alt: "Ellen Ripley"
          }
        }, {
          name: "Bruce Banner",
          yearsInCompany: 2,
          photo: {
            src: "./img/persona.jpg",
            alt: "Bruce Banner"
          }
        }, {
          name: "Éowyn",
          yearsInCompany: 5,
          photo: {
            src: "./img/persona.jpg",
            alt: "Éowyn"
          }
        }
      ]
    }

    render () {
        return html`
          <h2>Main</h2>
          <main>
            ${this.people.map(
              person => html`<persona-ficha-listado
               fname="${person.name}"
               yearsInCompany="${person.yearsInCompany}"
               .photo="${person.photo}"
              ></persona-ficha-listado>`
            )}
          </main>
        `;
    }
}

customElements.define('persona-main', PersonaMain);
