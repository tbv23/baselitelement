import { LitElement, html } from "lit-element";
import '../persona-header/persona-header.js';
import '../persona-main/persona-main.js';
import '../persona-footer/persona-footer.js';

class PersonaApp extends LitElement {

    render () {
        return html`
          <persona-header></persona-header>
          <persona-main></persona-main>
          <persona-footer></persona-footer>
        `;
    }
}

customElements.define('persona-app', PersonaApp);
