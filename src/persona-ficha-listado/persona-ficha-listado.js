import { LitElement, html } from "lit-element";

class PersonaFichaListado extends LitElement {

    static get properties() {
      return {
        fname: {type: String},
        yearsInCompany: {type: Number},
        photo: {type: Object}
      };
    }

    constructor() {
      super();
    }

    render () {
        return html`
          <div>
            <label>Nombre</label>
            <input type="text" value=${this.fname} />
            <br />
            <label>Años en la empresa</label>
            <input type="text" value=${this.yearsInCompany} />
            <br />
            <img src="${this.photo.src}" height="200" width="200"
              alt="${this.photo.alt}" >
          </div>
        `;
    }
}

customElements.define('persona-ficha-listado', PersonaFichaListado);
